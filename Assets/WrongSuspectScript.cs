﻿using UnityEngine;
using System.Collections;

public class WrongSuspectScript : MonoBehaviour {

	public GameObject ResignationLetter; 
	public GameObject RightSuspect;
	public GameObject Text; 


	void OnMouseDown () {
		ResignationLetter.SetActive (true); 
		RightSuspect.GetComponent<Collider2D> ().enabled = false; 
		Text.SetActive (true); 
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (ResignationLetter.activeSelf == true) {
			if (Input.GetKeyDown (KeyCode.R)){
				Application.LoadLevel (2); 	
			}
		}
	
	}
}
