﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EvidenceController : MonoBehaviour {

	public GameObject BigFeather; 
	public GameObject Text;
    public Text evidenceText;

	void OnMouseDown () {
		BigFeather.SetActive (true); 
		gameObject.GetComponent <Renderer> ().enabled = false;
        DataSave.evidenceCount = DataSave.evidenceCount + 1;
        gameObject.GetComponent<Collider2D>().enabled = false;
        //Text.SetActive (true); 
    }

	void OnMouseUp(){
		BigFeather.SetActive (false);
		gameObject.GetComponent <Renderer> ().enabled = true;
		//Text.SetActive (false); 
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        evidenceText.text = DataSave.evidenceCount.ToString();
        
	
	}

  
}
