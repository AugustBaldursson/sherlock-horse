﻿using UnityEngine;
using System.Collections;

public class OutsideController : MonoBehaviour {

	public GameObject Text; 
	public GameObject SpeechBubble; 

	void OnMouseDown () {
		if (SpeechBubble.activeSelf == false) {
			Application.LoadLevel (2);
		}
	}
	void OnMouseOver () {
		if (SpeechBubble.activeSelf == false) {
			Text.SetActive (true); 
		}
	}
	void OnMouseExit () {
		if (SpeechBubble.activeSelf == false) {
			Text.SetActive (false); 
		}
	}
	void Update () {
		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			SpeechBubble.SetActive (false);
		}

	}
}
