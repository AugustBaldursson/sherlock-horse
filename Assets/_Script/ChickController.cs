﻿using UnityEngine;
using System.Collections;

public class ChickController : MonoBehaviour {

	public GameObject Chick; 
	public GameObject Feather; 


	void OnMouseDown () {
		Chick.SetActive (true);
		Feather.SetActive (true); 
		gameObject.GetComponent<Collider2D> ().enabled = false;
		gameObject.GetComponent<Animator> () .enabled = true; 
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
